const request = require('request-promise-native')

const { getUniverse, getIncomingEnemyFleets } = require('./src/np')
const { getFleetName, getFleetOwner, getFleetDestinationStar, getShipsInFleet } = require('./src/fleet')
const { getPlayerName } = require('./src/player')
const { getStarName } = require('./src/star')

const username = process.env.USERNAME
const password = process.env.PASSWORD
const gameId = process.env.GAMEID
const pushoverToken = process.env.PUSHOVERTOKEN
const pushoverUser = process.env.PUSHOVERUSER

let universe = null

async function detectIncomingFleets() {
  if (!universe) { universe = await getUniverse(username, password, gameId) }
  const fleets = getIncomingEnemyFleets(universe)
  if (fleets.length === 0) {
    console.log(`No incoming fleets detected, rest easy`)
  } else {
    console.log(`!!!ALERT!!! ${fleets.length} incoming enemy fleets detected`)
    console.log(`-----`)
    for (const f of fleets) {
      console.log(`${getFleetName(f)} (${getPlayerName(getFleetOwner(universe, f))}) - inbound to ${getStarName(getFleetDestinationStar(universe, f))} with ${getShipsInFleet(f)} ships`)
    }
    await notify(`${fleets.length} incoming enemy fleets detected!`)
  }
}

async function notify(message) {
  return request.post('https://api.pushover.net/1/messages.json', {
    form: {
      token: pushoverToken,
      user: pushoverUser,
      message
    },
  })
}

detectIncomingFleets().catch(console.error)