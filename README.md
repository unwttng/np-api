# NP API

An API for a game I play.

## Install

```bash
$ yarn add np-api

$ npm i --save np-api
```

## Methods

```javascript
const { universe } = require('np-api')

universe('myadress@email.com', 'mynppassword', '1111111111111111').then(console.log)
```

`universe(username, password, gameId)` - request the game universe (filtered for your viewpoint)
