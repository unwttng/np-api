function isFleetInFlight(fleet) {
  return fleet.o.length > 0
}

function getFleetDestinationStar(universe, fleet) {
  return !isFleetInFlight(fleet) ? null : universe.stars[fleet.o[0][1]]
}

function getFleetId(fleet) {
  return fleet.uid
}

function getFleetName(fleet) {
  return fleet.n
}

function getFleetOriginStar(universe, fleet) {
  return !isFleetInFlight(fleet) ? null : universe.stars[fleet.o[1][1]]
}

function getFleetOwnerId(fleet) {
  return fleet.puid
}

function getFleetOwner(universe, fleet) {
  return universe.players[getFleetOwnerId(fleet)]
}

function getShipsInFleet(fleet) {
  return fleet.st
}

module.exports = {
  isFleetInFlight,
  getFleetDestinationStar,
  getFleetId,
  getFleetName,
  getFleetOriginStar,
  getFleetOwnerId,
  getFleetOwner,
  getShipsInFleet,
}
