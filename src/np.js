const _ = require('lodash')
const { log } = require('peasy-log')
const request = require('request-promise-native')

const { isFleetInFlight, getFleetDestinationStar, } = require('./fleet')

const API_BASE = `https://${['n', 'p', '.', 'i', 'r', 'o', 'n', 'h', 'e', 'l', 'm', 'e', 't' ].join('')}.com`
const AREQ_BASE = `${API_BASE}/a${'request'}`
const TREQ_BASE = `${API_BASE}/t${'request'}`
const GAME_BASE = `${API_BASE}/game`
const API_LOGIN = `${AREQ_BASE}/login`
const API_ORDER = `${TREQ_BASE}/order`

async function authenticate(username, password) {
  log(`_Authenticating_ with the server`)
  const resp = await request({
    url: API_LOGIN,
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/javascript, */*',
      'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
    },
    form: {
      type: 'login',
      alias: username,
      password,
    },
    resolveWithFullResponse: true,
  })
  if (resp.headers && resp.headers['set-cookie'] && resp.headers['set-cookie'].length > 0) {
    const cookieString = resp.headers['set-cookie'][0]
    log(`~~Successfully authenticated~~ with cookie **${cookieString}**`)
    return cookieString
  }
  throw 'Could not authenticate - problem encountered with login request'
}

async function getUniverse(username, password, gameNumber) {
  log(`_Getting_ universe data`)
  const auth = await authenticate(username, password)
  const resp = JSON.parse(await request({
    url: API_ORDER,
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/javascript, */*; q=0.01',
      'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
      'cookie': auth,
      'origin': API_BASE,
      'referer': `${GAME_BASE}/${gameNumber}`,
    },
    form: {
      type: 'order',
      order: 'full_universe_report',
      version: '',
      game_number: gameNumber,
    },
  }))
  log(`~~Successfully~~ got universe data for game **${resp.report.name}**`)
  return resp.report
}

function getPlayerId(universe) {
  return universe.player_uid
}

function getPlayerStars(universe, player = null) {
  const playerId = player || getPlayerId(universe)
  log(`_Identifying stars_ owned by player **${playerId}**`)
  const stars = _(universe.stars).filter(s => s.puid === playerId).value()
  log(`~~Identified~~ **${stars.length}** stars`)
  return stars
}

function getIncomingEnemyFleets(universe) {
  const playerId = getPlayerId(universe)
  log(`_Identifying incoming enemy fleets_ to player **${playerId}**'s stars`)
  const starIds = _.map(getPlayerStars(universe), s => s.uid)
  const fleets = _(universe.fleets)
    .reject(f => f.puid === playerId)  // Reject own fleets
    .filter(isFleetInFlight)
    .filter(f => _.includes(starIds, getFleetDestinationStar(universe, f).uid))  // Only those heading at player stars
    .value()
  log(`~~Identified~~ **${fleets.length}** incoming enemy fleets`)
  return fleets
}

module.exports = {
  getUniverse,
  getPlayerId,
  getPlayerStars,
  getIncomingEnemyFleets,
}
